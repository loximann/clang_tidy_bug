This code causes clang-tidy to crash because of a failed assertion. The error is:

.../clang/include/clang/StaticAnalyzer/Core/PathSensitive/SVals.h:105: T clang::ento::SVal::castAs() const [with T = clang::ento::nonloc::CompoundVal]: Assertion `T::isKind(*this)' failed


A bug report was filed here:

https://bugs.llvm.org/show_bug.cgi?id=39512

# How to reproduce

    git clone git@gitlab.com:loximann/clang_tidy_bug.git
    cd clang_tidy_bug
    mkdir build
    cd build
    cmake ..
    make

# Workarounds

Each workaround has been implemented in its own branch:

1. fix1_no_cxx17: Do not use C++17.
2. fix2_parens_constructor: Replace a certain use of T{} with T().
3. fix3_single_source_file: Move the content of module.cpp to main.cpp.