#include "module.h"

template<class T>
class MyClass {
public:
    MyClass() = default;

    void my_method(const T& x)
    {
        auto t = T{x * x};
        return;
    }
};

int main(int argc, char *argv[])
{
    MyClass<std::array<double, 3>> foo;
    foo.my_method(std::array<double, 3>{});

    return 0;
}
