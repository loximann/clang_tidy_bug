#ifndef MODULE_H
#define MODULE_H

#include <array>

/*** Some simple 3D vector arithmetics ***/
std::array<double, 3> operator*(const std::array<double, 3>& left, const std::array<double, 3>& right);

#endif // MODULE_H
