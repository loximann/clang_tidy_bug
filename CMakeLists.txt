set(CMAKE_CXX_STANDARD 17)

set(CMAKE_CXX_CLANG_TIDY clang-tidy CACHE STRING "")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_executable(clang_bug
    "${CMAKE_CURRENT_LIST_DIR}/module.cpp"
    "${CMAKE_CURRENT_LIST_DIR}/main.cpp"
)
